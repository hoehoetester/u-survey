import React, { Component } from 'react';
import fbSettings from './firebase.settings.js';
const firebase = require('firebase');
const uuid = require('uuid');


const config = fbSettings;
firebase.initializeApp(config);


class Usurvey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: uuid.v1(),
            studentName: '',
            answers: {
                answer1: '',
                answer2: '',
                answer3: ''
            },
            isSubmited: false
        };
        this.nameSubmit = this.nameSubmit.bind(this);
        this.answerSelected = this.answerSelected.bind(this);
        this.questionSubmit = this.questionSubmit.bind(this);
    }

    nameSubmit(e) {
        // debugger;
        var studentName = this.refs.name.value;
        this.setState({
            studentName: studentName
        }, () => console.log(this.state.studentName)
        );
    }

    answerSelected(e) {
        var answers = this.state.answers;
        if (e.target.name === 'answer1') {
            answers.answer1 = e.target.value;
        } else if (e.target.name === 'answer2') {
            answers.answer2 = e.target.value;
        } else if (e.target.name === 'answer3') {
            answers.answer3 = e.target.value;
        }
        this.setState({
            answers: answers
        }, () => console.log(this.state.answers));
    }

    questionSubmit() {
        firebase.database().ref('uservay/' + this.state.uid).set({
            studentName: this.state.studentName,
            answers: this.state.answers
        });
        this.setState({
            isSubmited: true
        });
    }

    render() {
        let studentName;
        let questions;

        if (this.state.studentName === '' && this.state.isSubmited === false) {
            studentName = <div>
                <h3>enter your name</h3>
                <form onSubmit={this.nameSubmit}>
                    <input type="text" placeholder="name" ref="name" required />
                    <input type="submit" className="btn" value="ok" />
                </form>
            </div>;
            questions = '';
        } else if (this.state.studentName !== '' && this.state.isSubmited === false) {
            studentName = <h3>welcome to usurvbey, {this.state.studentName}</h3>;
            questions = <div>
                <h3>here are some questions</h3>
                <form onSubmit={this.questionSubmit}>
                    <div className="card hoverable">
                        <h4>q1</h4>
                        <div className="radios">
                            <label className="radio-wrapper"><input type="radio" name="answer1" value="tech" onChange={this.answerSelected} /><span>tech</span></label>
                            <label className="radio-wrapper"><input type="radio" name="answer1" value="design" onChange={this.answerSelected} /><span>design</span></label>
                            <label className="radio-wrapper"><input type="radio" name="answer1" value="marketing" onChange={this.answerSelected} /><span>marketing</span></label>
                        </div>
                    </div>
                    <div className="card hoverable">
                        <h4>q2</h4>
                        <div className="radios">
                            <label className="radio-wrapper"><input type="radio" name="answer2" value="bad" onChange={this.answerSelected} /><span>bad</span></label>
                            <label className="radio-wrapper"><input type="radio" name="answer2" value="good" onChange={this.answerSelected} /><span>good</span></label>
                            <label className="radio-wrapper"><input type="radio" name="answer2" value="excellent" onChange={this.answerSelected} /><span>excellent</span></label>
                        </div>
                    </div>
                    <div className="card hoverable">
                        <h4>q3</h4>
                        <div className="radios">
                            <label className="radio-wrapper"><input type="radio" name="answer3" value="yes" onChange={this.answerSelected} /><span>yes</span></label>
                            <label className="radio-wrapper"><input type="radio" name="answer3" value="no" onChange={this.answerSelected} /><span>no</span></label>
                            <label className="radio-wrapper"><input type="radio" name="answer3" value="maybe" onChange={this.answerSelected} /><span>maybe</span></label>
                        </div>
                    </div>
                    <input type="submit" className="btn" value="submit" />
                </form>
            </div>;
        } else if (this.state.isSubmited === true) {
            studentName = <h3>thanks {this.state.studentName}!</h3>;
        }

        return (
            <div>
                {studentName}
                -------------------------------------------------------------<br />
                {questions}
            </div>
        );
    }
}

export default Usurvey;
